import Test.Hspec
import Test.QuickCheck
import RomanNumbers (r2i, i2r)

-- A QuickCheck generator for natural numbers between 1 and 10000.
naturals :: Gen Int
naturals = choose (1, 10000)

main :: IO ()
main = hspec $ do
  describe "RomanNumbers" $ do

    describe "r2i" $ do

      it "should convert \"I\" to 1" $ do
        r2i "I" `shouldBe` (1::Int)

      it "should convert \"XIV\" to 16" $ do
        r2i "XIV" `shouldBe` (16::Int)

      it "should convert \"X\" to 10" $ do
        r2i "X" `shouldBe` (10::Int)


    describe "i2r" $ do

      it "should convert 1 to \"I\"" $ do
        i2r (1::Int) `shouldBe` "I"

      it "should convert 16 to \"XIV\"" $ do
        i2r (16::Int) `shouldBe` "XVI"

      it "should convert 10 to \"X\"" $ do
        i2r (10::Int) `shouldBe` "X"


    describe "r2i . i2r" $ do

      it "(forall n : n in N : (r2i . i2r) n >= 1)" $ property $
        forAll naturals (\n -> (r2i . i2r) n >= (1::Int))

      it "should return the same number when composed" $ property $
        forAll naturals (\n -> (r2i . i2r) n == n)
