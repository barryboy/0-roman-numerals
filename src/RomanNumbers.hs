{-|
Module      : RomanNumbers
Description : Convert Roman numbers to integers and the other way around.
Copyright   : Alessandro Preiti (1774042)

RomanNumbers is a module to convert Roman numbers to integers and vice versa. Roman numbers are represented by the characters 'M', 'D', 'C', 'L', 'X', 'V', and 'I'
-}

module RomanNumbers
    ( -- * Convert Roman numbers to integers
      r2i
      -- * Convert integers to Roman numbers
    , i2r
    ) where

import Data.List (foldl')
import qualified Data.Map as Map

-- Implement and document r2i
romanList :: [(Char, Int)]
romanList = [
    ('O', 0),
    ('I', 1),
    ('V', 5),
    ('X', 10),
    ('L', 50),
    ('C', 100),
    ('D', 500),
    ('M', 1000)
  ]

-- Create the map from the list.
romanCharMap :: Map.Map Char Int
romanCharMap = Map.fromList romanList

-- Create the ordered list of (value, character) from the list.
romanValues :: [(Int, Char)]
romanValues = tail [(v, k) | (k, v) <- romanList]


charToInt :: Char -> Int
charToInt c = case Map.lookup c romanCharMap of
  Just val -> val
  Nothing -> error $ "Unexpected character; '" ++ [c] ++ "' is not a Roman numeral."

r2i :: String -> Int
r2i = sum . map charToInt

-- Implement and document i2r

-- Convert an integer to a Roman numeral using unfoldr.
i2r :: Int -> String
i2r n = reverse . snd $ foldl' unfoldRoman (n, []) (reverse romanValues)
  where
    unfoldRoman :: (Int, String) -> (Int, Char) -> (Int, String)
    unfoldRoman (remaining, acc) (value, ch) = (newRemaining, newAcc)
      where
        (count, newRemaining) = divMod remaining value
        newAcc = replicate count ch ++ acc
